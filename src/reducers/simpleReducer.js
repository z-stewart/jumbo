import * as types from '../actions/types';

export default (state = {}, action) => {
    switch (action.type) {
        case (types.BEGIN_AJAX_CALL) :
            switch(action.target){
                case types.GET_MOVIE_DETAILS: {

                    return Object.assign({}, state, {
                        loadingDetails: true
                    });
                }
                default:
                    return state;
            }

        case (types.AJAX_CALL_SUCCESS) :
            switch(action.target){
                case types.START_MOVIE_SEARCH: {
                    return Object.assign({}, state, {
                        movieResultList: action.result.results,
                        pageNumber: action.result.page,
                        totalPages: action.result.total_pages
                    });
                }
                case types.GET_MORE_MOVIE_SEARCH_RESULTS: {
                    let list = state.movieResultList ? state.movieResultList.concat(action.result.results) : action.result.results;
                    return Object.assign({}, state, {
                        movieResultList: list,
                        pageNumber: action.result.page,
                        totalPages: action.result.total_pages
                    });
                }
                case types.GET_MOVIE_DETAILS: {

                    return Object.assign({}, state, {
                        movieDetails: action.result,
                        loadingDetails: false
                    });
                }
            default:
                return state;
            }

        case types.CLEAR_MOVIE_DETAILS:
            return Object.assign({}, state, {
                movieDetails: null
            });

        default:
            return state
    }
}