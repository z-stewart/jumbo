import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import configureStore from './store';
import * as serviceWorker from './serviceWorker';
import MovieDetails from "./containers/MovieDetails";
import {BrowserRouter as Router, Route} from "react-router-dom";
import {Provider} from 'react-redux';

ReactDOM.render(
    <Provider store={configureStore()}>
        <Router >
            <div>
                <Route path={'/'} exact component={App} />
                <Route path={'/detail'} component={MovieDetails} />
            </div>
        </Router>
    </Provider>,

    document.getElementById('root')
);

serviceWorker.unregister();
