import React from 'react';
import PropTypes from "prop-types";
import {connect} from 'react-redux';
import { getSearch, getMoreSearchResults, getMovieDetails } from "./actions/actions";
import { bindActionCreators } from 'redux';
import moment from 'moment';
import './App.css';
import {Link} from "react-router-dom";

import { createBrowserHistory } from 'history';
const history = createBrowserHistory();


class App extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.search = this.search.bind(this);
        this.update = this.update.bind(this);
        this.loadMore = this.loadMore.bind(this);
        this.detailsClicked = this.detailsClicked.bind(this);

        this.state = {
            searchText: ""
        };

    }

    search() {
        this.props.getSearch(this.state.searchText);
    }

    update(event) {
        event.preventDefault();
        let value = event.target.value;
        this.setState({
            searchText: value
        });
    }

    loadMore() {
        this.props.getMoreSearchResults(this.state.searchText, this.props.pageNumber+1);
    }

    detailsClicked(id) {
        this.props.getMovieDetails(id);
        this.props.history.push('/detail');
    }


    render() {

        let list = [];
        for (let i = 0; i < this.props.movieResultList.length; i++) {
            let result = this.props.movieResultList[i];
            let classPop = "movieItemPopularity good";
            let pop = Math.floor(result.vote_average)*10;
            if(pop < 60){
                classPop += " average"
            }
            if(pop < 40){
                classPop += " belowAverage"
            }
            if(pop < 20){
                classPop += " bad"
            }
            list.push(<div key={i} className="movieGridItem">
                    <div className="poster">
                        <span className={classPop}>{Math.floor(result.vote_average)*10}%</span>
                        <img alt="poster" onClick={() => this.detailsClicked(result.id)} src={"https://image.tmdb.org/t/p/w500" + result.poster_path} />
                    </div>
                    <div className="movieTitle">{result.original_title}</div>
                    <div className="movieDetails">{moment(result.release_date).format('MMM Y')}</div>
                </div>
            )
        }
        return (
            <div className="App">
                <div className="moviesSearchResults">
                    <div className="header main">
                        <img alt="logo" className="logo" src={require('./moveDatabase.svg')}/>
                    </div>
                    <div className="searchArea">
                        <input onChange={this.update} placeholder="Search" onKeyPress={this.search} value={this.state.searchText}/>
                        <img alt="search" className="searchIcon"  onClick={this.search}  src={require('./search.png')}/>
                    </div>
                    <h2>Popular Movies</h2>
                    <div className="movieGrid">
                        {list}
                    </div>
                    {this.props.totalPages !== this.props.pageNumber &&
                    <button onClick={this.loadMore}>Load More</button>
                    }
                </div>

            </div>
        );
    }
}

App.defaultProps = {
    movieResultList: []
};

App.propTypes = {
    getSearch: PropTypes.func,
    getMoreSearchResults: PropTypes.func,
    getMovieDetails: PropTypes.func,
    clearMovieDetails: PropTypes.func,
    movieResultList: PropTypes.array,
    movieDetails: PropTypes.object,
    totalPages: PropTypes.number,
    pageNumber: PropTypes.number
};
function mapDispatchToProps(dispatch) {
    return {
        getSearch: bindActionCreators(getSearch, dispatch),
        getMoreSearchResults: bindActionCreators(getMoreSearchResults, dispatch),
        getMovieDetails: bindActionCreators(getMovieDetails, dispatch)
    };
}
function mapStateToProps(state) {
    return {
        movieResultList: state.simpleReducer.movieResultList,
        totalPages: state.simpleReducer.totalPages,
        pageNumber: state.simpleReducer.pageNumber,
        movieDetails: state.simpleReducer.movieDetails
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(App);
