import * as types from './types';
import api from '../api/api';
import {beginAjaxCall, ajaxCallSuccess, ajaxCallError} from './ajaxStatusActions';

export function getSearch(searchText){
    return function(dispatch) {
        dispatch(beginAjaxCall(types.START_MOVIE_SEARCH));

        return api.getSearch(searchText).then(result => {
            dispatch(ajaxCallSuccess(types.START_MOVIE_SEARCH, result));
        }).catch(error => {
            dispatch(ajaxCallError(types.START_MOVIE_SEARCH, error));
        });
    };
}
export function getMoreSearchResults(searchText, pageNumber){
    return function(dispatch) {
        dispatch(beginAjaxCall(types.GET_MORE_MOVIE_SEARCH_RESULTS));

        return api.getMoreSearchResults(searchText, pageNumber).then(result => {
            dispatch(ajaxCallSuccess(types.GET_MORE_MOVIE_SEARCH_RESULTS, result));
        }).catch(error => {
            dispatch(ajaxCallError(types.GET_MORE_MOVIE_SEARCH_RESULTS, error));
        });
    };
}
export function getMovieDetails(id){
    return function(dispatch) {
        dispatch(beginAjaxCall(types.GET_MOVIE_DETAILS));

        return api.getMovieDetails(id).then(result => {
            dispatch(ajaxCallSuccess(types.GET_MOVIE_DETAILS, result));
        }).catch(error => {
            dispatch(ajaxCallError(types.GET_MOVIE_DETAILS, error));
        });
    };
}

export function clearMovieDetails() {

    return {
        type: types.CLEAR_MOVIE_DETAILS
    };
}
