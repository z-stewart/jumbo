import * as types from './types';

export function beginAjaxCall(target, data){
    return {
        type: types.BEGIN_AJAX_CALL,
        target,
        data
    };
}

export function ajaxCallSuccess(target, result, data){
    return {
        type: types.AJAX_CALL_SUCCESS,
        target,
        result,
        data
    };
}

export function ajaxCallError(target, error, data){
    return {
        type: types.AJAX_CALL_ERROR,
        target,
        error,
        data
    };
}

export function apiUnavailable(){
    return {
        type: types.AJAX_API_UNAVAILABLE
    };
}
