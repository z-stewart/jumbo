import jQuery from 'jquery'
class api {
    constructor() {
        this.get = this.get.bind(this);
    }

    static getSearch(searchText, pageNumber = 1) {
        return this.get("https://api.themoviedb.org/3/search/movie?api_key=6ed12e064b90ae1290fa326ce9e790ff&language=en-US&query="+searchText+"&page="+pageNumber+"&include_adult=false");
    }

    static getMoreSearchResults(searchText, pageNumber) {
        return this.get("https://api.themoviedb.org/3/search/movie?api_key=6ed12e064b90ae1290fa326ce9e790ff&language=en-US&query="+searchText+"&page="+pageNumber+"&include_adult=false");
    }
    static getMovieDetails(id) {
        return this.get("https://api.themoviedb.org/3/movie/" + id + "?api_key=6ed12e064b90ae1290fa326ce9e790ff&language=en-US");
    }

    static get(url) {
        return new Promise((resolve, reject) => {
            return jQuery.ajax({
                url: url
            }).done((data) => {
                resolve(data);
            }).fail(reject);
        });
    }
}
export default api;


