import React from 'react';
import PropTypes from "prop-types";
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import moment from 'moment';
import '../App.css';
import { clearMovieDetails } from "../actions/actions";

class MovieDetails extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.back = this.back.bind(this);
    }

    back() {
        this.props.history.push('./');
        this.props.clearMovieDetails();
    }
    componentWillMount() {
        if(!this.props.movieDetails && !this.props.loadingDetails){
            this.back();
        }
    }

    render() {
        return (
            <div className="movieItemDetails">
                {this.props.loadingDetails &&
                    <div className="lds-ring">
                        <div></div> <div></div> <div></div> <div></div>
                    </div>
                }
                {this.props.movieDetails &&
                <div>
                    <img alt="back" className="backIcon" src={require('../back.png')} onClick={this.back}/>
                    <div className="header">
                        <img alt="backdrop" src={"https://image.tmdb.org/t/p/w500" + this.props.movieDetails.backdrop_path}/>
                    </div>
                    <div className="movieImageAndTitle">
                        <div className="poster"><img alt="poster" src={"https://image.tmdb.org/t/p/w500" + this.props.movieDetails.poster_path} /></div>
                        <div className="movieDetailsTitle">
                            <h2 className="movieTitle">{this.props.movieDetails.original_title}</h2>
                            <div className="movieDetails">
                                <span className="releaseDate">{moment(this.props.movieDetails.release_date).format('Y')}</span>
                                <span>{Math.floor(this.props.movieDetails.vote_average) *10}% User Score</span>
                                <div>{this.props.movieDetails.runtime} mins</div>
                            </div>
                        </div>
                    </div>
                    <hr/>
                    <div className="overview">
                        <h3>Overview</h3>
                        <p>{this.props.movieDetails.overview}</p>
                    </div>
                </div>
                }
            </div>
        );
    }
}

MovieDetails.propTypes = {
    movieDetails: PropTypes.object,
    loadingDetails: PropTypes.bool
};

function mapStateToProps(state) {
    return {
        movieDetails: state.simpleReducer.movieDetails,
        loadingDetails: state.simpleReducer.loadingDetails
    }
}
function mapDispatchToProps(dispatch) {
    return {
        clearMovieDetails: bindActionCreators(clearMovieDetails, dispatch)
    };
}


export default connect(mapStateToProps, mapDispatchToProps)(MovieDetails);

